from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def expenses_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/expenses.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category": category,
    }
    return render(request, "receipts/list.html", context)


def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account": account,
    }
    return render(request, "receipts/accounts.html", context)


def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
