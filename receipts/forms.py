from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt

        fields = (
            "vendor",
            "total",
            "tax",
            "purchaser",
            "date",
            "category",
            "account",
        )


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = (
            "name",
            "owner",
        )


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
            "owner",
        )
