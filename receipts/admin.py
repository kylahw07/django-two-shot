from django.contrib import admin

# Register your models here.
from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "date",
        "purchaser",
        "category",
        "account",
    )
