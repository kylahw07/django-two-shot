from django.urls import path
from accounts.views import loginview, logoutview, signupview

urlpatterns = [
    path("signup/", signupview, name="signup"),
    path("logout/", logoutview, name="logout"),
    path("login/", loginview, name="login"),
]
